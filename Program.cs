﻿using System;

namespace Ejercicio1
{
    class Carro
    {
        // Atributos
        public string Marca;
        public string Modelo;
        public string Version;
        public int Anio;
        public float Precio;
         
        // Métodos
        public void Caracteristicas()
        {
            Console.WriteLine("Marca : " + this.Marca);
            Console.WriteLine("Modelo : " + this.Modelo);
            Console.WriteLine("Versión :" + this.Version);
            Console.Write("Anio :" );
            Console.WriteLine(this.Anio);
            Console.Write("Precio :");
            Console.WriteLine(this.Precio);
            return;
        }

        public void Financiamiento(int Meses)
        {
            Console.Write("El financiamiento es de 0 meses y 0% de Enganche y cada mes de ");
            Console.WriteLine(this.Precio/12);
        }

        public void Seguro()
        {
            Console.WriteLine("El precio del seguro es $12,000.00 pesos anuales");
            return;
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            Carro Yaris = new Carro();
            Yaris.Marca = "Toyota";
            Yaris.Modelo = "Yaris";
            Yaris.Version = "CVT";
            Yaris.Anio = 2017;
            Yaris.Precio = 225000;
            Yaris.Caracteristicas();
            Yaris.Seguro();
            Yaris.Financiamiento(12);
        }
    }
}
